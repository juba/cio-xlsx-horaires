import locale
from lib import data
from lib import workbook


# PARAMÈTRES --------

DATE_START = "2024-09-02"  # (?)
DATE_END = "2025-07-04"  # (?)
WEEKS_CLOSED = [43, 44, 52, 1, 9, 10, 17, 18]  # (?)
DAYS_CLOSED = [
    "2024-11-01",  # 1er novembre
    "2024-11-11",  # 11 novembre
    "2024-12-25",  # Noël
    "2025-01-01",  # Jour de l'an
    "2025-04-21",  # Lundi de Pâques
    "2025-05-01",  # 1er mai
    "2025-05-08",  # 8 mai
    "2025-05-29",  # Ascension
    "2025-05-30",  # Pont de l'Ascension (?)
    "2025-06-09",  # Lundi de Pentecôte (?)
]

XLSX_FILE = "heures.xlsx"

if __name__ == "__main__":
    # Locale
    locale.setlocale(locale.LC_ALL, "")

    # Data
    calendar = data.WeekData(DATE_START, DATE_END, WEEKS_CLOSED, DAYS_CLOSED)

    # Génération workbook
    workbook = workbook.WeekWorkbook(XLSX_FILE, calendar)
    workbook.generate_workbook()
    workbook.close()
