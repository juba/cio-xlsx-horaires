Outil de génération de fichier Excel pour saisie des horaires des PsyEN d'un CIO.

Éditer les paramètres dans `genere_xls.py` et lancer :

```sh
uv run python genere_xls.py
```
