from xlsxwriter.utility import xl_rowcol_to_cell
from datetime import datetime, timedelta


class Month:
    @staticmethod
    def first_day_of_week(year, week):
        """Return the date of the first day of a week"""
        return datetime.strptime(f"{year}-{week}-1", "%Y-%W-%w").date()

    def __init__(self, calendar, sheet, month, i_month, formats, config):
        self.calendar = calendar
        self.sheet = sheet
        self.month = month
        self.i_month = i_month
        self.formats = formats
        self.config = config
        self.total_cells = {}
        self.hebdo_cells = {}
        self.week_url_cells = {}

    def day_is_active(self, date):
        """Compute if a date is active or closed"""
        active = (
            date >= self.calendar.date_start
            and date <= self.calendar.date_end
            and (date not in self.calendar.days_closed)
        )
        return active

    def write_week_headers(self, sheet, row, col, week, i_month):
        """Write headers of a week table."""
        sheet.write_blank(row, col - 1, None, self.formats["right_border"])
        formt = self.formats[f"week_header{i_month}"]

        sheet.merge_range(
            row,
            col,
            row,
            col + 1,
            f"Semaine {week}",
            formt,
        )
        col = col + 2
        sheet.write(row, col, "Lieu", formt)
        col = col + 1
        sheet.write(row, col, "Début", formt)
        col = col + 1
        sheet.write(row, col, "Fin", formt)
        col = col + 1
        sheet.write(row, col, "Heures", formt)

    def write_week_days(self, sheet, row, col, week_start, i_month):
        """Write day lines of a week table"""
        count_active = 0
        for day in range(0, 5):
            date = week_start + timedelta(days=day)
            day_row = row + day * 2 + 1
            sheet.write_blank(day_row, col - 1, None, self.formats["right_border"])
            sheet.write_blank(day_row + 1, col - 1, None, self.formats["right_border"])
            sheet.merge_range(
                day_row,
                col,
                day_row + 1,
                col,
                datetime.strftime(date, "%A %d %B"),
                self.formats[f"day_date{i_month}"],
            )
            sheet.write(day_row, col + 1, "Matin", self.formats[f"day_am{i_month}"])
            sheet.write(
                day_row + 1,
                col + 1,
                "Après-midi",
                self.formats[f"day_pm{i_month}"],
            )

            active = self.day_is_active(date)
            if active:
                count_active += 1

            for i in (0, 1):
                cur_row = day_row + i
                format_suffix = "_bottom" if i == 1 else ""
                if active:
                    sheet.write_blank(
                        cur_row,
                        col + 2,
                        None,
                        self.formats[f"place{format_suffix}"],
                    )
                    sheet.data_validation(
                        xl_rowcol_to_cell(cur_row, col + 2),
                        {
                            "validate": "list",
                            "source": self.config["etablissements_cells"],
                        },
                    )
                    sheet.write_blank(
                        cur_row,
                        col + 3,
                        None,
                        self.formats[f"hour{format_suffix}"],
                    )
                    sheet.write_blank(
                        cur_row,
                        col + 4,
                        None,
                        self.formats[f"hour{format_suffix}"],
                    )
                    cell_start = xl_rowcol_to_cell(cur_row, col + 3)
                    cell_end = xl_rowcol_to_cell(cur_row, col + 4)
                    sheet.write_formula(
                        cur_row,
                        col + 5,
                        f"=IF({cell_end}-{cell_start}>0,{cell_end}-{cell_start},0)",
                        self.formats[f"hour_calc{i_month}{format_suffix}"],
                        "",
                    )
                else:
                    sheet.merge_range(
                        cur_row,
                        col + 2,
                        cur_row,
                        col + 5,
                        "",
                        self.formats[f"hour_inactive{format_suffix}"],
                    )
        return count_active

    def write_week_totals(self, sheet, row, col, week, week_id, active_days):
        """Write total hour lines for a week table"""
        # Ajout total heures semaine
        sheet.merge_range(
            row + 11,
            col + 3,
            row + 11,
            col + 4,
            f"Total semaine {week} :",
            self.formats["week_total"],
        )
        sheet.write_formula(
            row + 11,
            col + 5,
            f"=SUM({xl_rowcol_to_cell(row+1, col+5)}:{xl_rowcol_to_cell(row+10, col+5 )})",
            self.formats["week_total_value"],
            "",
        )
        # On garde en mémoire la position du total de la semaine
        self.total_cells[week_id] = xl_rowcol_to_cell(row + 11, col + 5)

        # Ajout heures hebdo semaine
        sheet.merge_range(
            row + 12,
            col + 3,
            row + 12,
            col + 4,
            f"Heures semaine {week} :",
            self.formats["week_hebdo"],
        )
        sheet.write_formula(
            row + 12,
            col + 5,
            f"=Bilan!{self.config['hebdo_hours_cell']}*{active_days}/5",
            self.formats["week_hebdo_value"],
            "",
        )
        # On garde en mémoire la position du nombre d'heures de la semaine
        self.hebdo_cells[week_id] = xl_rowcol_to_cell(row + 12, col + 5)

    def write_week(self, sheet, month, i_week, week, week_start, i_month):
        """Write a week table."""

        # Starting row
        row = 15 * (i_week // 2) + 2
        # Starting col
        col = 1 if i_week % 2 == 0 else 8

        # Week id
        week_id = f"{month['month']}-{month['year']}-{week}"

        # Store week position for internal link in bilan table
        self.week_url_cells[week_id] = xl_rowcol_to_cell(row, col)

        # Week table headers
        self.write_week_headers(sheet, row, col, week, i_month)
        # Days
        active_days = self.write_week_days(sheet, row, col, week_start, i_month)
        # Totals
        self.write_week_totals(sheet, row, col, week, week_id, active_days)

    def write_month(self):
        """Format and write a month worksheet"""
        sheet = self.sheet
        month = self.month
        i_month = self.i_month
        year = month["year"]

        # Format sheet
        sheet.protect()
        sheet.set_row(0, 25)
        for col in (0, 7):
            sheet.set_column(col, col, 4)
            sheet.set_column(col + 1, col + 1, 26)
            sheet.set_column(col + 2, col + 2, 13)
            sheet.set_column(col + 3, col + 3, 20)
            sheet.set_column(col + 4, col + 5, 10)
            sheet.set_column(col + 6, col + 6, 9)

        # Write sheet content
        sheet.write(
            0,
            1,
            f"{month['month_name']} {year}".title(),
            self.formats["month_title"],
        )
        for i_week, week in enumerate(month["weeks"]):
            week_start = Month.first_day_of_week(year, week)
            self.write_week(sheet, month, i_week, week, week_start, i_month)
