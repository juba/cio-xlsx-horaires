import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell, xl_range
from .formats import Formats
from .month import Month
from .bilan import Bilan


class WeekWorkbook:
    def __init__(self, filename, calendar):
        # Dictionnaires pour garder les coordonnées des cellules des données hebdo
        self.total_cells = {}
        self.hebdo_cells = {}
        self.week_url_cells = {}
        self.config = {
            "etab_default": [
                "CIO",
                "Autre",
                "Établissement 1",
                "Établissement 2",
                "Établissement 3",
                "Établissement 4",
            ],
            # Default week work time : 27h
            "hebdo_hours_value": 1.125,
            "hebdo_hours_cell": "D3",
            "etablissements_cells": "=Bilan!$J$6:$J$11",
            # Lieu columns in month sheets
            "cols_lieu": ["D", "K"],
            # Hour columns in month sheets
            "cols_hours": ["G", "N"],
            # Values updated while building
            "total_cells": {},
            "hebdo_cells": {},
            "week_url_cells": {},
        }

        # Création workbook
        self.wb = xlsxwriter.Workbook(filename)
        self.calendar = calendar

        # Formats
        self.formats = self.init_formats()

    def init_formats(self):
        """Define and store workbook formats"""
        n_months = len(self.calendar.months)
        formats = Formats(n_months).formats
        formats = {k: self.wb.add_format(f) for k, f in formats.items()}
        return formats

    def generate_workbook(self):
        """Generate whole workbook"""
        self.sheet_bilan = self.wb.add_worksheet("Bilan")
        for i_month, month in enumerate(self.calendar.months):
            sheet_name = f"{month.get('month_name')} {month.get('year')}"
            sheet = self.wb.add_worksheet(sheet_name)
            month = Month(
                self.calendar, sheet, month, i_month, self.formats, self.config
            )
            month.write_month()
            for item in ("total_cells", "hebdo_cells", "week_url_cells"):
                getattr(self, "config")[item].update(getattr(month, item))

        bilan = Bilan(
            self.calendar,
            self.sheet_bilan,
            self.formats,
            self.config,
            self.wb.worksheets(),
        )
        bilan.write_bilan()

    def close(self):
        """Close workbook"""
        self.wb.close()
