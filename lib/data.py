import calendar
from datetime import datetime, date


class WeekData:
    @staticmethod
    def month_of_week(year, week):
        """
        Returns the month of a week for a given year. Month of week is computed
        as the month of the wednesday.
        """
        wednesday = datetime.strptime(f"{year}-{week}-3", "%Y-%W-%w").date()
        return wednesday.month

    def __init__(self, date_start, date_end, weeks_closed, days_closed):
        self.date_start = date.fromisoformat(date_start)
        self.date_end = date.fromisoformat(date_end)

        self.first_year = self.date_start.year
        self.second_year = self.date_end.year
        self.first_months_range = range(self.date_start.month, 13)
        self.second_months_range = range(1, self.date_end.month + 1)

        self.weeks_closed = weeks_closed
        self.days_closed = list(map(date.fromisoformat, days_closed))

        self.months = self.generate_calendar()

    def first_weeks(self):
        """Computes the range of weeks of the first year"""
        start_week = self.date_start.isocalendar()[1]
        last_week = date(self.first_year, 12, 28).isocalendar()[1]
        return range(start_week, last_week + 1)

    def second_weeks(self):
        """Computes the range of weeks of the second year"""
        start_week = 1
        last_week = self.date_end.isocalendar()[1]
        return range(start_week, last_week + 1)

    def calendar_year(self, type):
        """
        Computes a calendar object for the `type` year (first or second).
        The object contains, for each month, the month number, month name,
        year, and list of week numbers.
        """
        year = self.first_year if type == "first" else self.second_year
        months_range = (
            self.first_months_range
            if type == "first"
            else self.second_months_range
        )
        weeks_range = (
            self.first_weeks() if type == "first" else self.second_weeks()
        )
        cal = [
            {
                "month": i,
                "month_name": calendar.month_name[i],
                "year": year,
                "weeks": [],
            }
            for i in months_range
        ]
        for week in weeks_range:
            if week in self.weeks_closed:
                continue
            week_month = self.month_of_week(year, week)
            found = False
            for m in cal:
                if m["month"] == week_month:
                    m["weeks"].append(week)
                    found = True
            # If not found we are at the first week of first month
            if not (found):
                cal[0].get("weeks").append(week)
        return cal

    def generate_calendar(self):
        """Returns the calendar object for both years."""
        first = self.calendar_year("first")
        second = self.calendar_year("second")
        return first + second
