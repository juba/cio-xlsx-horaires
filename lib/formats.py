import colorsys
import numpy as np


class Formats:
    @staticmethod
    def gen_palette(hue):
        """
        Generate a list of three hex colors at different values within a hue.
        """
        return [
            "#%02x%02x%02x"
            % tuple(
                [round(i * 255) for i in colorsys.hsv_to_rgb(hue, saturation, value)]
            )
            for (saturation, value) in zip((0.6, 0.3, 0.15), (0.5, 0.95, 1.0))
        ]

    def __init__(self, n_months):
        """Generate workbook formats."""
        self.n_months = n_months
        self.formats = {}
        self.build_global_formats()
        self.build_week_formats()

    def build_global_formats(self):
        """
        Build global celle formats.
        """
        hour_bg_color = "#FEFFAD"
        inactive_bg_color = "#EEEEEE"
        inactive_font_color = "#777777"
        hebdo_bg_color = "#FFAE6A"
        total_week_bg_color = "yellow"
        bg_bilan_altern_color = "#EEEEFF"
        formats = {
            "month_title": {"bold": True, "font_size": 18},
            "hour": {
                "num_format": "hh:mm",
                "locked": False,
                "bg_color": hour_bg_color,
            },
            "hour_bottom": {
                "num_format": "hh:mm",
                "locked": False,
                "bg_color": hour_bg_color,
                "bottom": 1,
            },
            "hour_inactive": {
                "align": "center",
                "font_color": inactive_font_color,
                "bg_color": inactive_bg_color,
                "right": 1,
            },
            "hour_inactive_bottom": {
                "align": "center",
                "font_color": inactive_font_color,
                "bg_color": inactive_bg_color,
                "right": 1,
                "bottom": 1,
            },
            "place": {"locked": False, "bg_color": hour_bg_color, "right": 1},
            "place_bottom": {
                "locked": False,
                "bg_color": hour_bg_color,
                "right": 1,
                "bottom": 1,
            },
            "week_total": {"bold": True, "align": "right"},
            "week_total_value": {
                "bold": True,
                "align": "right",
                "num_format": "[h]:mm",
                "bg_color": total_week_bg_color,
            },
            "week_hebdo": {"bold": True, "align": "right"},
            "week_hebdo_value": {
                "bold": True,
                "align": "right",
                "num_format": "[h]:mm",
                "locked": False,
                "bg_color": hebdo_bg_color,
            },
            "right_border": {"right": 1},
            "bilan_header": {
                "bold": True,
                "align": "center",
                "bottom": 1,
                "top": 1,
                "right": 1,
                "bg_color": "#333333",
                "font_color": "#FFFFFF",
                "valign": "vcenter",
                "text_wrap": True,
            },
            "bilan_month": {
                "align": "center",
                "right": 1,
                "valign": "vcenter",
                "bottom": 1,
                "bold": True,
            },
            "bilan_month_altern": {
                "align": "center",
                "right": 1,
                "valign": "vcenter",
                "bottom": 1,
                "bold": True,
                "bg_color": bg_bilan_altern_color,
            },
            "bilan_week": {"align": "center", "right": 1},
            "bilan_week_bottom": {"align": "center", "right": 1, "bottom": 1},
            "bilan_week_altern": {
                "align": "center",
                "right": 1,
                "bg_color": bg_bilan_altern_color,
            },
            "bilan_week_altern_bottom": {
                "align": "center",
                "right": 1,
                "bottom": 1,
                "bg_color": bg_bilan_altern_color,
            },
            "bilan_hour_hebdo": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": hebdo_bg_color,
            },
            "bilan_hour_hebdo_bold": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": hebdo_bg_color,
                "bold": True,
            },
            "bilan_hour_hebdo_bottom": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bottom": 1,
                "bg_color": hebdo_bg_color,
            },
            "bilan_hour": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": total_week_bg_color,
            },
            "bilan_hour_bottom": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bottom": 1,
                "bg_color": total_week_bg_color,
            },
            "bilan_hour_sup": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": "#E0E0E0",
            },
            "bilan_hour_sup_bottom": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": "#E0E0E0",
                "bottom": 1,
            },
            "bilan_hour_sup_cumul": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": "#FFFFFF",
            },
            "bilan_hour_sup_cumul_bottom": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": "#FFFFFF",
                "bottom": 1,
            },
            "bilan_etablissement_hours": {
                "num_format": "[h]:mm",
                "right": 1,
                "align": "center",
                "bg_color": "#FFFFFF",
                "locked": False,
                "bottom": 1,
            },
            "quotite_label": {"align": "right"},
            "quotite_value": {
                "bold": True,
                "num_format": "[h]:mm",
                "locked": False,
                "align": "center",
                "bg_color": hebdo_bg_color,
            },
        }
        self.formats.update(formats)

    def build_week_formats(self):
        """
        Build week cell formats for each month worksheet.
        """
        formats = {}
        n_colors = self.n_months
        hues = np.linspace(0, 1 - (1 / n_colors), n_colors)
        np.random.shuffle(hues)
        for i, hue in enumerate(hues):
            dark, mid, light = Formats.gen_palette(hue)
            this_format = {
                f"week_header{i}": {
                    "bold": True,
                    "align": "center",
                    "bottom": 1,
                    "top": 1,
                    "right": 1,
                    "bg_color": dark,
                    "font_color": "#FFFFFF",
                },
                f"day_date{i}": {
                    "bold": True,
                    "align": "center",
                    "valign": "vcenter",
                    "bottom": 1,
                    "right": 1,
                    "bg_color": mid,
                },
                f"day_am{i}": {
                    "align": "center",
                    "right": 1,
                    "bg_color": light,
                },
                f"day_pm{i}": {
                    "align": "center",
                    "bottom": 1,
                    "right": 1,
                    "bg_color": light,
                },
                f"hour_calc{i}": {
                    "num_format": "[h]:mm",
                    "left": 1,
                    "right": 1,
                    "bg_color": light,
                },
                f"hour_calc{i}_bottom": {
                    "num_format": "[h]:mm",
                    "left": 1,
                    "bottom": 1,
                    "right": 1,
                    "bg_color": light,
                },
            }
            formats.update(this_format)
        self.formats.update(formats)
