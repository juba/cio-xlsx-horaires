from xlsxwriter.utility import xl_rowcol_to_cell, xl_range


class Bilan:
    def __init__(self, calendar, sheet, formats, config, worksheets):
        self.calendar = calendar
        self.sheet = sheet
        self.formats = formats
        self.config = config
        self.worksheets = worksheets

    def write_bilan_table_headers(self, row):
        """Write headers of the table on bilan sheet"""
        self.sheet.write_blank(row, 0, None, self.formats["right_border"])
        self.sheet.write(row, 1, "Mois", self.formats["bilan_header"])
        self.sheet.write(row, 2, "Semaine", self.formats["bilan_header"])
        self.sheet.write(row, 3, "Heure\nhebdo", self.formats["bilan_header"])
        self.sheet.write(row, 4, "Heures\nréelles", self.formats["bilan_header"])
        self.sheet.write(row, 5, "Heures\nmanquantes", self.formats["bilan_header"])
        self.sheet.write(row, 6, "Heures\nsup", self.formats["bilan_header"])
        self.sheet.write(row, 7, "Heures sup\ncumulées", self.formats["bilan_header"])

    def write_bilan_table_row(
        self, row, i_month, month, i_week, week, last_week, start_row
    ):
        """Write a row of the table on the bilan sheet"""
        sheet = self.sheet

        format_suffix = "_bottom" if last_week else ""
        altern_suffix = "_altern" if i_month % 2 == 0 else ""

        week_id = f"{month['month']}-{month['year']}-{week}"

        self.sheet.write_blank(row, 0, None, self.formats["right_border"])

        if last_week:
            if i_week > 0:
                sheet.merge_range(
                    row - i_week,
                    1,
                    row,
                    1,
                    f"{month['month_name']} {month['year']}",
                    self.formats["bilan_month" + altern_suffix],
                )
            else:
                sheet.write(
                    row,
                    1,
                    f"{month['month_name']} {month['year']}",
                    self.formats["bilan_month" + altern_suffix],
                )

        week_url = f"internal:'{month['month_name']} {month['year']}'!{self.config['week_url_cells'][week_id]}"
        sheet.write_url(
            row,
            2,
            week_url,
            self.formats["bilan_week" + altern_suffix + format_suffix],
            str(week),
        )

        hebdo_ref_cell = self.config["hebdo_cells"][week_id]
        hebdo_ref = f"='{month['month_name']} {month['year']}'!{hebdo_ref_cell}"
        sheet.write_formula(
            row,
            3,
            hebdo_ref,
            self.formats["bilan_hour_hebdo" + format_suffix],
            "",
        )

        total_ref_cell = self.config["total_cells"][week_id]
        total_ref = f"='{month['month_name']} {month['year']}'!{total_ref_cell}"
        sheet.write_formula(
            row, 4, total_ref, self.formats["bilan_hour" + format_suffix], ""
        )
        # Colonne heures manquantes
        cell_thq = xl_rowcol_to_cell(row, 3)
        cell_eff = xl_rowcol_to_cell(row, 4)
        sheet.write_formula(
            row,
            5,
            f'=IF({cell_eff}=0,"",IF({cell_eff}-{cell_thq}<0,{cell_thq}-{cell_eff},""))',
            self.formats["bilan_hour_sup" + format_suffix],
            "",
        )
        # Colonne heures sup
        sheet.write_formula(
            row,
            6,
            f'=IF({cell_eff}=0,"",IF({cell_eff}-{cell_thq}>=0,{cell_eff}-{cell_thq},""))',
            self.formats["bilan_hour_sup" + format_suffix],
            "",
        )
        # Colonne heures sup cumulées
        range_manq = xl_range(start_row + 1, 5, row, 5)
        range_sup = xl_range(start_row + 1, 6, row, 6)
        cumul_formula = f"SUM({range_sup})-SUM({range_manq})"
        cumul_formula_reverse = f"SUM({range_manq})-SUM({range_sup})"
        sheet.write_formula(
            row,
            7,
            f'=IF({cumul_formula}<0, TEXT({cumul_formula_reverse},"-[h]:mm"), {cumul_formula})',
            self.formats["bilan_hour_sup_cumul" + format_suffix],
            "",
        )

    def write_bilan_conditional_format(self, start_row, end_row):
        """Write bilan conditional formatting on bilan sheet"""
        # bg and color for heures sup
        max_color_manq = "#46C92B"
        max_color_sup = "#FF5555"
        cond_range = xl_range(start_row + 1, 5, end_row, 5)
        self.sheet.conditional_format(
            cond_range,
            {
                "type": "2_color_scale",
                "min_type": "num",
                "min_value": 0,
                "min_color": "#FFFFFF",
                "max_type": "num",
                "max_value": 0.3,
                "max_color": max_color_manq,
            },
        )
        cond_range = xl_range(start_row + 1, 6, end_row, 6)
        self.sheet.conditional_format(
            cond_range,
            {
                "type": "2_color_scale",
                "min_type": "num",
                "min_value": 0,
                "min_color": "#FFFFFF",
                "max_type": "num",
                "max_value": 0.3,
                "max_color": max_color_sup,
            },
        )
        cond_range = xl_range(start_row + 1, 7, end_row, 7)
        self.sheet.conditional_format(
            cond_range,
            {
                "type": "3_color_scale",
                "mid_type": "num",
                "mid_value": 0,
                "mid_color": "white",
                "min_type": "num",
                "min_value": -2,
                "min_color": max_color_manq,
                "max_type": "num",
                "max_value": 2,
                "max_color": max_color_sup,
            },
        )

    def write_etablissements_formula(self, row, col):
        """Generate the formula to compute the number of hours by établissement."""
        # Months worksheets
        sheets = self.worksheets[1:]
        # Cell with etablissement name
        etab_cell = xl_rowcol_to_cell(row, col - 1)
        form = [
            f"SUMIF('{sheet.name}'!{cols[0]}:{cols[0]},Bilan!{etab_cell},'{sheet.name}'!{cols[1]}:{cols[1]})"
            for sheet in sheets
            for cols in zip(self.config["cols_lieu"], self.config["cols_hours"])
        ]
        form = "+".join(form)
        form = "=" + form
        self.sheet.write_formula(
            row,
            col,
            form,
            self.formats["bilan_etablissement_hours"],
        )

    def write_etablissements_table(self, row, col):
        """Write the établissments table in the bilan worksheet."""
        self.sheet.merge_range(
            row, 8, row + 6, col - 1, None, self.formats["right_border"]
        )
        # Headers
        self.sheet.write(row, col, "Établissements", self.formats["bilan_header"])
        self.sheet.write(row, col + 1, "Total", self.formats["bilan_header"])
        # Cells
        etab_default = self.config["etab_default"]
        for i in range(len(etab_default)):
            self.sheet.write(
                row + i + 1,
                col,
                etab_default[i],
                self.formats["place_bottom"],
            )
            self.write_etablissements_formula(row + i + 1, col + 1)

    def write_bilan(self):
        """Format and write the bilan worksheet"""
        sheet = self.sheet

        sheet.protect()
        # Title height
        sheet.set_row(0, 25)
        # Headers height
        sheet.set_row(4, 30)
        # Columns width
        sheet.set_column(0, 0, 4)
        sheet.set_column(1, 1, 25)
        sheet.set_column(2, 2, 13)
        sheet.set_column(3, 3, 13)
        sheet.set_column(4, 4, 13)
        sheet.set_column(5, 5, 13)
        sheet.set_column(6, 6, 13)
        sheet.set_column(7, 7, 13)
        sheet.set_column(9, 9, 25)
        sheet.set_column(10, 10, 13)

        # Title
        row = 0
        sheet.write(
            row,
            1,
            "Bilan",
            self.formats["month_title"],
        )

        # Write default hebdo hours
        row = 2
        sheet.merge_range(
            row,
            1,
            row,
            2,
            "Temps de travail hebdo par défaut :",
            self.formats["quotite_label"],
        )
        sheet.write(
            self.config["hebdo_hours_cell"],
            self.config["hebdo_hours_value"],
            self.formats["quotite_value"],
        )

        # Write table headers
        row = 4
        start_row = row
        self.write_bilan_table_headers(start_row)

        # Write table
        for i_month, month in enumerate(self.calendar.months):
            for i_week, week in enumerate(month["weeks"]):
                row += 1
                last_week = i_week == len(month["weeks"]) - 1
                self.write_bilan_table_row(
                    row, i_month, month, i_week, week, last_week, start_row
                )
        # Write conditional formatting
        self.write_bilan_conditional_format(start_row, row)

        # Write établissements table
        row = 4
        col = 9
        self.write_etablissements_table(row, col)
